# Docker for Laravel 5.5

## Packages

 - PHP-FPM 7.1
 - Nginx 
 - MySQL 8 
 - Memcached 
 - PhpMyAdmin

## Getting Started

1. Copy docker-compose.yml.dist to docker-compose.yml
2. Install images and containers
```
docker-compose up -d
```

### Usage

Start Docker containers:
```
docker-compose up -d
```

Stop Docker containers:
```
docker-compose stop
```